import React from 'react';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import Layout from './components/Layout';
import PokemonList from './pages/PokemonList';
import PokemonDetail from './pages/PokemonDetails';

function App() {
  return (
    <Router>
      <Layout>
        <Routes>
          <Route path="/" exact component={PokemonList} />
          <Route path="/pokemon/:id" component={PokemonDetail} />
        </Routes>
      </Layout>
    </Router>
  );
}

export default App;