// Fichier PokemonList.js
import React, { useEffect, useState } from 'react';
import axios from 'axios';
import Cards from '../components/Cards';
import Layout from '../components/Layout';

export default function PokemonList() {
  const [pokemons, setPokemons] = useState([]);
  const [count, setCount] = useState(0);

  useEffect(() => {
    axios.get(`https://pokeapi.co/api/v2/pokemon?offset=${count}&limit=10`)
      .then((response) => {
        setPokemons((prevPokemons) => [...prevPokemons, ...response.data.results]);
        setCount(count + 10);
      })
      .catch((error) => {
        console.error(`Erreur lors de la récupération des données de l'API Pokémon : ${error}`);
      });
  }, [count]);

  return (
    <Layout>
      <div>
        {pokemons.map((pokemon, index) => (
          <Cards key={index} pokemon={pokemon} />
        ))}
        <button onClick={() => setCount(count + 10)}>Charger plus</button>
      </div>
    </Layout>
  );
}