import React from 'react';
import { Link } from 'react-router-dom';

export default function Cards({ pokemon }) {
  return (
    <div>
      <h2>{pokemon.name}</h2>
      <Link to={`/pokemon/${pokemon.name}`}>Voir les détails</Link>
    </div>
  );
}