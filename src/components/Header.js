import React, { useState } from 'react';
import { NavLink } from 'react-router-dom';

function Header() {
  const [search, setSearch] = useState('');

  const handleSearch = (e) => {
    e.preventDefault();
    console.log(search);
    
  };

  return (
    <div>
      <header className='bg-blue-600 flex items-center justify-between px-10 py-6'>
        <NavLink to="/">
          <h1 className='text-2xl text-white font-semibold'>Pokemon</h1>
        </NavLink>

        <nav>
          <ul className='flex items-center gap-10 text-xl text-white'>
            <li><NavLink to="/">Home</NavLink></li>
            <li><NavLink to="/pokedex">Pokedex</NavLink></li>
            <li><NavLink to="/videogame">Video Game</NavLink></li>
            <li><NavLink to="/news">News</NavLink></li>
          </ul>
        </nav>

        <form className='flex items-center gap-2' onSubmit={handleSearch}>
          <input className='px-2 h-[44px] rounded-l' type="text" value={search} onChange={(e) => setSearch(e.target.value)} />
          <button className='text-xl rounded-r bg-white text-blue-600 px-4 py-2 flex items-center' type="submit">Search</button>
        </form>
      </header>
    </div>
  );
}

export default Header;